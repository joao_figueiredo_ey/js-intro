define(["app/models/company","app/models/companydetails"],
	function(Company, CompanyDetails){
		function companySearch(value, renderTable){

			return getListData(value).then(listHandler).catch(errorHandler)
		}

		function companyInfo(value){
			getDetailsData(value, detailHandler)
		}

		function errorHandler(error){
	    	console.log(error)
	    	return error;
	    	/*
	    	if(error){
	    		throw new error;   		
	    	}*/
	    }

	    function listHandler(results){
			return results.data.map(function(data){ 
				return new Company(data.ticker, data.name) 
			});
	    }

		function detailHandler(error, results, callBackFunction){
	    	if(error){
	    		throw new Error(error);   		
	    	}
	    	
	    	company = new CompanyDetails(results.name,results.ceo,results.business_address,results.long_description);
			
			requirejs(['app/controllers/details'], function(controller){
				controller.start(company);
			});
	    }

	    function getListData(value) {
	    	return new Promise(function(resolve, reject){
	    		var username = '3c16c0895fe5f3f0f94428cb932c95f5';
		        var password = '56aa0007a49ba036b30d8c436fd4e2fb';
		        var path = "/companies?query=" + value;
		        var auth = "Basic " + b64EncodeUnicode(username + ':' + password);

		        $.ajax({
		            headers: {
		                'Authorization': auth
		            },
		            url: 'https://api.intrinio.com' + path,
		            type: 'GET',
		            dataTtpe: 'json',
		            success: resolve,
		            error: reject
		        }); 
	    	});  
	    }

	    function getDetailsData(value, cb) {
	        var username = '3c16c0895fe5f3f0f94428cb932c95f5';
	        var password = '56aa0007a49ba036b30d8c436fd4e2fb';
	        var path = "/companies?ticker=" + value;
	        var auth = "Basic " + b64EncodeUnicode(username + ':' + password);

	        $.ajax({
	            headers: {
	                'Authorization': auth
	            },
	            url: 'https://api.intrinio.com' + path,
	            type: 'GET',
	            dataTtpe: 'json',
	            success: function(results){
	            	cb(null, results)
	            },
	            error:function(request){
	                cb(request.responseText)
	            }
	        });   
	    }

	    // Taken from https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#Solution_2_%E2%80%93_rewrite_the_DOMs_atob()_and_btoa()_using_JavaScript's_TypedArrays_and_UTF-8
	    function b64EncodeUnicode(str) {
	        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
	            function toSolidBytes(match, p1) {
	                return String.fromCharCode('0x' + p1);
	        }));
	    }

		return {
			companySearch: companySearch,
			companyInfo: companyInfo
		}
	}
);