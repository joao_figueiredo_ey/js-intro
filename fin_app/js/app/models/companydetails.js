define([],
	function(){
		function CompanyDetails(name, ceo, business_address, long_description) {
		    this.ceo = ceo;
		    this.name = name;
		    this.business_address = business_address;
		    this.long_description = long_description;
		}

		return CompanyDetails
	}
)