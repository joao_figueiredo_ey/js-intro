define([],
	function(){
		function render(searchData, tableData){
			console.log(!searchData)
			if(searchData){
				drawSearchTab();
			}

			if(tableData){
				drawTable(tableData);				
			}
		}

		function drawSearchTab(){
			$('#app').html('<div id="search" style="text-align:center;margin-top: 20px">'+
							'Choose a Company: <br>'+
							'<input id="search_input"></input>'+
							'<br>'+
							'<button id="search_button" style="text-align:center; margin-top: 5px">Search</button>'+
							'</div>'+
							'<div id="list" style="text-align:center;margin-top: 50px"><table id="list_table" align="center"></table></div>');
		}

		function drawTable(tableData){
			$('#list_table').html('<thead><tr>'+
    						'<th>Ticker</th>'+
    						'<th>Name</th>'+
    						'<th></th>'+
  							'</tr></thead><hr><tbody>');
			
			tableData.forEach(function(row){
				drawRow(row);
			});

			$('#list_table').append('</tbody>');
		}

		function drawRow(row){
			$('#list_table').append('<tr>'+
	    						'<th>'+row.ticker+'</th>'+
	    						'<th style="text-align:left;padding:0px 20px">'+row.name+'</th>'+
	    						'<th><button id="detail_button" data-ticker='+
	    						row.ticker+'>Details</button></th>'+
	  							'</tr>');
		}

		function searchClickHandler(handler){
			$("#search_button").click(handler);
		}

		function searchDetailHandler(handler){
			$("[id='detail_button']").click(handler);
		}

		return{
			render: render,
			bindSearch: searchClickHandler,
			bindDetails: searchDetailHandler
		}
	}
);