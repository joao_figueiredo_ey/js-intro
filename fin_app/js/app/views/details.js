define([],
	function(){
		function render(companyData){
			if(companyData){
				currentHash = '#details'
				window.location.hash = currentHash
				drawInfo(companyData);
			}
		}

		function drawInfo(data){
			$('#app').html('<div style="text-align:center;margin-top: 20px">'+
							'<button id="back_button">Back</button>'+
							'<br>'+'<br>'+
							data.name+'<br>'+
							data.ceo+'<br>'+
							data.business_address+'<br>'+
							data.long_description+'</div>');
		}

		function bindReturn(handler){
			$("#back_button").click(handler);
		}

		return{
			render: render,
			bindReturn: bindReturn
		}
	}
);