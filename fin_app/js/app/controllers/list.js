define(["app/views/list", "app/services/company_info_service"],
	function(listView, companyInfoService){
		function start(){
			listView.render('InitialSearch',null);
			listView.bindSearch(searchHandler);
		}

		function renderTable(data){
			listView.render(null, data);
			listView.bindDetails(detailsHandler);
		}

		function searchHandler(event){

			companyInfoService.companySearch($('#search_input').val()).then(renderTable);
		}

		function detailsHandler(event){
			companyInfoService.companyInfo(event.target.dataset.ticker)
		}

		return {
			start: start
		}
	}
);