define(["app/views/details", "app/services/company_info_service"],
	function(detailsView, companyInfoService){
		function start(params){
			detailsView.render(params)
			detailsView.bindReturn(returnHandler);
		}

		function returnHandler(event){
			window.location.hash = '#list'
		}

		return {
			start: start
		}
	}
);