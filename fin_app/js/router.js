/*router:
	delegar nos controllers

	window.location.hash -> o router está sempre à escuta das alterações deste metodo

	main require router e document.ready(router.start())
*/

define(
	function(){

		var routes = {
			list: {
				hash: '#list',
				controller: 'list'
			},
			details: {
				hash: '#details',
				controller: 'details'
			},
			statment: {
				hash: '#statment',
				controller: 'statement'
			}
		}

		var defaultUrl = 'list'
		var currentHash = ''

		function start() {
			window.location.hash = window.location.hash || routes[defaultUrl].hash

			setInterval(checkHash, 100);
		}
		
		function checkHash() {
			if(window.location.hash === currentHash){
				return;
			}

			routeName = Object.keys(routes).find(function(elem){
				return window.location.hash === routes[elem].hash
			})

			if(!routeName){
				goToDefaultController();
				return;
			}

			goToController(routes[routeName].controller);
		}
		
		function goToController(controller) {
			currentHash = window.location.hash;

			requirejs(['app/controllers/' + controller], function(controller){
				controller.start();
			})		
		}
		
		function goToDefaultController() {
			window.location.hash = routes[defaultUrl].hash

			goToController(routes[defaultUrl].controller)
		}

		return{
			start: start
		}

	}
);
