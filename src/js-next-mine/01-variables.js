/**
 * Should return array of number returning functions from 0 to times
 * Use block scope (not closure) to fix this
 */
exports.numberReturningFuncs = function(times) {

    var fs = [];

    for (var i = 0; i < times; i++) {
        let cnt = i

        fs.push(() => cnt);
    }

    return fs;
};

/**
 * Prevent variable reassignment
 */
exports.reassignVariable = function() {

    const a = 1;

    {
        const a = 1;
        a = 2;
    }
};
