/**
 * Create a Prefixer class with constructor and a prefixArray method
 */
exports.Prefixer = class Prefixer{
    constructor(prefix){
	    this.prefix = prefix;
    }

    prefixArray(arr) {
    	return arr.map((arg) => this.prefix + arg )
    }
};

/**
 * Create a PrefixerSuffixer class which extends from the Prefixer class
 * but adds a sufixArray method
 */
exports.PrefixerSufixer = class PrefixerSufixer extends this.Prefixer {
	constructor(prefix, sufix){
	    super(prefix);
	    this.sufix = sufix;
    }

	sufixArray(arr) {
    	return arr.map((arg) => arg + this.sufix )
    }
};
