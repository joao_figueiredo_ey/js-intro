/**
 * Fix the async retreive method, doing proper error handling
 * in case fetch does not return a user
 */
exports.User = class {

    constructor(url) {
        this.url = url;
    }

    async retreive() {
    	const response = await fetch(this.url);

    	if(!response.name){
    		throw new Error
    	}

    	return response
    }
};
