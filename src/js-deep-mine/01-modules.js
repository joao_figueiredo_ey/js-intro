/**
 * Creates a counter module with an initial value, zero if not provided
 */
exports.createCounter = function(counter) {
	
	var counter = counter || 0

	return {
		get: function(){
			return counter
		},
		increment: function(){
			counter += 1
		},
		reset: function(){
			counter = 0
		}
	};

};

/**
 * Creates a person module with name and age
 * An initial name value should be provided and
 * an exception thrown if not
 */
exports.createPerson = function(name) {
	var name_sanitizer = name.match(/[A-Za-z]/g)

	var age = 0

	if(name_sanitizer === null){
		throw new Error('No name here...')
	}else{
		return {
			    getName: function(){
					return name
				},
				getAge: function(){
					return age
				},
				setAge: function(new_age){
					age = new_age
				}, 
		}
	};
};
