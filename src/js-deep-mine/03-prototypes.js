/**
 * Create a developer person object with a code method
 * that delegates to the provided person object
 */
exports.createDelegate = function(person) {
	var output = Object.create(person);

	output.code = function(){ console.log("ze") };

	return output
};

/**
 * Borrow and invoke the person say method on top of the company object
 */
exports.borrowSayMethod = function(person, company) {
	return person.say.call(company)
};

/**
 * Iterate over all of the provided object own properties,
 * returning an array of key: value strings
 */
exports.iterate = function(obj) {
	var output = Object.keys(obj)

	Object.values(obj).forEach(function(el, idx){
		output[idx] = output[idx] + ": " + el
	});

	return output
};

/**
 * Add a repeatify method to all JavaScript Strings
 * 'string'.repeatify(3) should equal 'stringstringstring'
 * NOTE: This exercise exists for a pedagogic purpose only,
 * do not think doing stuff like this is a good idea...
 *
 * String already contains a repeat method, let's NOT use that one.
 */
exports.extendString = function() {
	String.prototype.repeatify = function(x) {
		var output = ''

		for(var i = 0; i < x; i++) {
			output = output + this
		}

		return output
	}
};
