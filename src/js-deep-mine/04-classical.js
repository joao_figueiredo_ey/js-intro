/**
 * Return a Rectangle constructor function
 * Rectangle instances should contain x and y properites
 * and they should know how to calculate their area and perimeter
 */
exports.createRectangleConstructor = function() {
	return function Rectangle(x, y){
		this.x = x; 
		this.y = y;
		this.perimeter = false;
		this.area = false;

		return {
			perimeter: function(){
				return 2 * (this.x + this.y)
			},
			area: function(){
				return this.x * this.y
			}
		} 
	}
};

/**
 * Return a constructor function that
 * subclasses the provided constructor
 */
exports.createSubClass = function(fn) {
};


