/**
 * Return result of calling function with arguments as present in array
 */
exports.argsAsArray = function(fn, arr) {
	return fn.apply(null, arr);
};

/**
 * Return the result of calling a function with the provided obj as context
 * for the implicit argument this
 */
exports.speak = function(fn, obj) {
	return fn.apply(obj); //ou .call
};

/**
 * Return a prefixer function that concatenates a string argument with a prefix
 */
exports.stringPrefixer = function(prefix) {
	var ze = prefix

	return function(word){
		return ze + word
	};
};

/**
 * Create an array of functions, each producing a result obtained
 * from applying the transform function to an argument from values array
 */
exports.makeResultFunctions = function(values, transform) {
	
	return values.map(
		function(element) {
			return function() {
				return transform.call(null, element);
			}
		}
	);

/*
	var output_arr = []

	values.forEach(function(element) {
		output_arr.push(function() {
			return transform.call(null, element);
		});
	});

	return output_arr
*/
};

/**
 * From a function which receives three arguments,
 * of which only two are available, create a new function
 * which wraps the original one with the missing argument
 */
exports.createWrapperFunction = function(fn, arg1, arg2) {
	return function(ze) {
		return fn.call(this, arg1, arg2, ze);
	};
};

/**
 * Return the sum of all arguments
 */
exports.sumArguments = function() {
	const reducer = (accumulator, currentValue) => accumulator + currentValue;

	return [].slice.call(arguments).reduce(reducer);
};

/**
 * Execute the function, passing it all the provided arguments
 */
exports.callIt = function(fn) {
	var args = [].slice.call(arguments);
	
	args.shift();

	return fn.apply(this, args);
};

/**
 * From a function which receives a variable number of arguments,
 * return a wrapper function which adds additional argumens to the ones
 * available and calls the original function
 */
exports.createAddArgumentsFunction = function(fn) {
	var outside_args = [].slice.call(arguments);
	
	outside_args.shift();

	return function() {
		var overall_args = outside_args.concat([].slice.call(arguments));

		return fn.apply(this, overall_args)
	}
};
