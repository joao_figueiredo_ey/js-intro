/**
 * Invoke the callback and return the amount of time in miliseconds it took to execute
 */
exports.profileFunc = function(cb) {
	var time_init = Date.now();
	cb();
	return Date.now() - time_init;
};

/**
 * Invoke the async callback with the provided value after some delay
 */
exports.returnWithDelay = function(value, delay, cb) {
	var callbackFunction = function() {
		return cb(null, value);
	}

	setTimeout(callbackFunction, delay);
};

/**
 * Invoke the async callback with an error after some delay
 */
exports.failWithDelay = function(delay, cb) {
	var callbackFunction = function() {
		return cb(new Error("Fire!"));
	}

	setTimeout(callbackFunction, delay);
};

/**
 * Return a promise that resolves after the specified delay
 * or rejects if the delay is negative or non-existent
 */
exports.promiseBasedDelay = function(delay) {
	cb = function(resolve, reject){
		if(!delay || delay < 0){
			reject();
		}
		else{
			setTimeout(resolve, delay);
		} 	
	}

	return new Promise(cb);
};

/**
 * Use fetch to grab the contents of both urls and return
 * a promise resolving to the payload concatenation
 */
exports.urlToUpperCase = function(url1, url2) {

	cb = function(result){
		return fetch(url2)
			.then(function(result2){
				return result + result2
			}
		);
	};

	return fetch(url1).then(cb);
};