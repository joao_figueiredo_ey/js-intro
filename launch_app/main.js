$(document).ready(function(){
  main();
});

async function main() {
  try{
    response = await fetchNextLaunches();
  }catch(err){
    errConstruction(err);
  }
  htmlConstruction(response);
};

async function fetchNextLaunches() {

  const api = 'http://launchlibrary.net/1.3/launch/next/16';
  const response = await fetch(`${api}`);
  const rockets = await response.json();  

  if(!rockets.launches){
    throw new Error('no launches');
  };
console.log(rockets.launches);
  return rockets.launches;
};

function htmlConstruction(response){
  response.forEach((response) =>
    $('#container').append(`<a href="${response.vidURLs[0]}" style="padding: 10px">
                              <div class="ui link card very padded text container segment">
                                  <div class="content">
                                  <div class="header">${response.name}</div>
                                  <div class="meta">
                                    <span class="category"></span>
                                  </div>
                                  <div class="description">
                                    <p>${response.rocket.agencies[0].name}</p>
                                  </div>
                                </div>
                                <div class="extra content">
                                  <div class="right floated author">
                                  <i class="rocket icon"></i>
                                  </div>
                                </div>
                              </div>
                            </a>`)
  );
};

function errConstruction(err){

};