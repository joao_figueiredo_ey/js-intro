import axios from 'axios'

export async function getLanguagesData() {
  const url = 'https://api.github.com/search/repositories?q=stars:%3E1+language:javascript&sort=stars&order=desc&type=Repositories'

  return await axios.get(url)
                    .catch(function (error) {
                             console.log(error);
                          });
};