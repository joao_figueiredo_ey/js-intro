import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ButtonGroup from './ButtonGroup';
import TabGroup from './TabGroup';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">AllStar App</h1>
        </header>
        <div className={'App-body'}>
          <ButtonGroup />
          <TabGroup />
        </div>
      </div>      
    );
  }
}

export default App;
