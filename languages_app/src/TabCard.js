import React from 'react';
import { Card, Icon, Image } from 'semantic-ui-react';
//style={{width: 100, height: 100, justifyContent: 'center', alignItems: 'center'}}

const TabCard = (props) => (
			<Card>
      <Image src={props.item.owner.avatar_url} />
      <Card.Content>
        <Card.Header>{props.item.owner.login}</Card.Header>
        <Card.Description>{props.item.language}</Card.Description>
      	<a>
          <Icon name='star' />
          {props.item.watchers_count}
        </a>
      </Card.Content>
    </Card>
)

export default TabCard;