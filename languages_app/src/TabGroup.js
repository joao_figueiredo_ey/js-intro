import React, { Component } from 'react';
import TabCard from './TabCard';
import { Grid } from 'semantic-ui-react';
import MockData from './dummy';
import { getLanguagesData } from './GitHubService';

class TabGroup extends Component {
  state = {repos: []};

  static data = MockData.items
  
  async componentDidMount(){
  	const data2 = await getLanguagesData()

  	this.setState({repos: data2.data.items});	
  }

  render() {
  	console.log(this.state);
    return(
    	<div>
	    	<Grid columns={6}>
		  		{this.state.repos.map(rep => {
					return(
						<Grid.Column key={rep.name}>
						    <TabCard
	                            key={rep.name}
	                            item={rep}
	                            onClick={ () => { this.selectLanguage(rep) } }
	                        />
	                    </Grid.Column>
                    )
		        })}
	        </Grid>
      	</div>
    )
  }
}

export default TabGroup;