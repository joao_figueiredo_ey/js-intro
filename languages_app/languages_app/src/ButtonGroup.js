import React, { Component } from 'react';
import ButtonItem from './ButtonItem';
import { Button } from 'semantic-ui-react';

class ButtonGroup extends Component {
  static languages = ['All', 'JavaScript', 'Java', 'HTML', 'CSS', 'C', 'C++'];
  state = { selected: 'All' };

  selectLanguage = (lang) => {
    this.setState({ selected: lang });
  }  

  render() {
    return (
      <div className={'App-body'}>
        <Button.Group>
          {ButtonGroup.languages.map(lang => {
            return(
              <ButtonItem
                onClick={ () => { this.selectLanguage(lang) } }
                key={lang}
                item={lang}
              />
            );
          })}
        </Button.Group>
      </div>
    );
  }
}

export default ButtonGroup;
