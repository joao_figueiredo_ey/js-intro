import React from 'react';
import { Button } from 'semantic-ui-react';

function ButtonItem(props){
	return <Button onClick={ props.onClick }>{ props.item }</Button>
};

export default ButtonItem;